image: registry.gitlab.com/buildstream/buildstream-docker-images/testsuite-fedora:30-master-80990368
# Testsuite image at 6e2d93c1f9132f207d0abbf822eaf5f76677533c (09/09/2019)

cache:
  paths:
    - cache/

stages:
  - test
  - docs
  - upload

before_script:
  # Diagnostics
  - mount
  - df -h

  - adduser -m buildstream
  - chown -R buildstream:buildstream .

#####################################################
#                    Test stage                     #
#####################################################

integration_linux:
  stage: test
  variables:
    PYTEST_ADDOPTS: "--color=yes"
  script:

    - export INTEGRATION_CACHE="$(pwd)/cache/integration-cache"

    - chown -R buildstream:buildstream .

    # Run the tests from the source distribution.
    # We run as a simple user to test for permission issues.
    - su buildstream -c 'tox'

integration_unix:
  stage: test
  variables:
    BST_FORCE_SANDBOX: "chroot"
    PYTEST_ADDOPTS: "--color=yes"
  script:

    # We remove the Bubblewrap and OSTree packages here so that we catch any
    # codepaths that try to use them. Removing OSTree causes fuse-libs to
    # disappear unless we mark it as user-installed.
    - dnf mark install fuse-libs systemd-udev
    - dnf erase -y bubblewrap ostree

    - export INTEGRATION_CACHE="$(pwd)/cache/integration-cache"

    # Since the unix platform is required to run as root, no user change required
    - tox

  artifacts:
    paths:
    - logs-unix/

lint:
  stage: test
  script:
  - tox -e lint

# Automatically build documentation, only for merges which land
# on master branch.
#
# Note: We still do not enforce a consistent installation of python2
#       or sphinx, as python2 will significantly grow the backing image.
#

#####################################################
#                  Docs stage                       #
#####################################################

pages:
  stage: docs
  script:
  - dnf install -y python2 make
  # Pin sphinx as docs will not build at latest version
  - pip3 install sphinx==1.8.5
  - pip3 install sphinx-click
  # Pin sphinx_rtd_theme because search functionality breaks
  # at the latest version
  - pip3 install sphinx_rtd_theme>=0.4.2
  - pip3 install --user .
  - make -C doc
  - mv doc/build/html public
  artifacts:
    paths:
    - public/
  only:
  - master


# Automatically upload to PyPI when a release is tagged.

#####################################################
#               Upload stage                        #
#####################################################

upload:
  stage: upload
  script:
  - python3 setup.py sdist
  - twine upload dist/*.tar.gz
  only:
  - /[0-9]+\.[0-9]+\.[0-9]+/
  except:
  - branches
